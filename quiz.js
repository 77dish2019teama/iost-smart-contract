class QuizContract {
    init() {}

    createQuiz(
        quizId,
        quizName,
        sponsor,
        prizePool,
        questionHashListStr) {
        if (storage.mapHas("quiz", quizId.toString())) {
            throw "Quiz with ID " + quizId + " already exists";
        }

        blockchain.deposit(tx.publisher, prizePool + 5, `Prize pool and 5 iost fee for quiz. ID: ${quizId} Prize Pool: ${prizePool}`);
        blockchain.withdraw(blockchain.contractOwner(), 5, `Payment for quiz creation. ID: ${quizId} Prize Pool: ${prizePool}`);

        let quiz = {
            "quizId": quizId,
            "quizName": quizName, 
            "sponsor": sponsor,
            "sponsorAccount": tx.publisher, 
            "prizePool": prizePool,
            "questionHashList": JSON.parse(questionHashListStr),
            "finished": false
        }

        storage.mapPut("quizzes", quizId.toString(), JSON.stringify(quiz));
        return "Successfully added quiz with id " + quizId;
    }
    
    recordCorrectAnswer(
        quizId, 
        questionNumber, 
        questionHash, 
        answerHash, 
        pubKey) {
        let quizJsonStr = storage.mapGet("quizzes", quizId.toString());

        if (!quizJsonStr) {
            throw "Quiz with ID " + quizId + " does not exist";
        }

        if (!(blockchain.requireAuth(blockchain.contractOwner(), "active") || 
                blockchain.requireAuth(quiz.sponsorAccount, "active"))) {
            throw "Only available for contract owner or sponsor";
        }

        let quiz = JSON.parse(quizJsonStr);

        if (quiz.finished) {
            throw "Quiz is already finished";
        }

        if (questionNumber <= 0 || questionNumber > quiz.questionHashList.length) {
            throw "Invalid question number";
        }

        let questionInfo = quiz.questionHashList[questionNumber - 1];
        if (questionInfo.questionHash === questionHash &&
            questionInfo.answerHash === answerHash) {

            if (questionNumber !== 1) {
                let prevQuestionNumber = questionNumber - 1;
                let prevLogList = JSON.parse(storage.mapGet("answer_logs", `(${quizId}, ${prevQuestionNumber})`));
                if (prevLogList.indexOf(pubKey) < 0) {
                    throw "User is not qualified to answer this question";
                }
            }

            let logListStr = storage.mapGet("answer_logs", `(${quizId}, ${questionNumber})`);
            let logList = [];
            if (logListStr) {
                logList = JSON.parse(logListStr);
            }

            if (logList.indexOf(pubKey) < 0) {
                logList.push(pubKey);
                storage.mapPut("answer_logs", `(${quizId}, ${questionNumber})`, JSON.stringify(logList));
                return "Successfully added entry";
            }

            throw "User already logged";
        }
        throw "Invalid question hashes";
    }

    getQuizDetails(quizId) {
        let quizJsonStr = storage.mapGet("quizzes", quizId.toString());

        if (!quizJsonStr) {
            throw "Quiz with ID " + quizId + " does not exist";
        }
        return quizJsonStr;
    }

    markQuizFinished(quizId) {
        if (!(blockchain.requireAuth(blockchain.contractOwner(), "active") || 
                blockchain.requireAuth(quiz.sponsorAccount, "active"))) {
            throw "Only available for contract owner or sponsor";
        }

        let quizJsonStr = storage.mapGet("quizzes", quizId.toString());
        if (!quizJsonStr) {
            throw `Quiz with ID ${quizId} does not exist`;
        }

        let quiz = JSON.parse(quizJsonStr);
        if (quiz.finished) {
            throw `Quiz with ID ${quizId} is already finished`;
        }

        let lastQuestionNumber = quiz.questionHashList.length;
        let winnerList = JSON.parse(storage.mapGet("answer_logs", `(${quizId}, ${lastQuestionNumber})`));

        if (winnerList.length === 0) {
            blockchain.withdraw(quiz.sponsorAccount, quiz.prizePool, `No winner for quiz id: ${quizId}`);
            return "No winners, returned prizePool to sponsor";
        }

        let prizePerWinner = quiz.prizePool / winnerList.length;

        for (let winner of winnerList) {
            blockchain.withdraw(winner, prizePerWinner, `Prize for winning quiz id: ${quizId}`);
        }

        quiz.finished = true;
        storage.mapPut("quizzes", quizId.toString(), JSON.stringify(quiz));
        return "Successfully disbursed rewards to winners";
    }

    getQuizQuestionWinners(quizId, questionNumber) {
        let logListStr = storage.mapGet("answer_logs", `(${quizId}, ${questionNumber})`);
        if (logListStr) {
            return logListStr;
        }

        throw "Quiz ID and Question Number combination does not exist";
    }

    can_update(data) {
        return blockchain.requireAuth(blockchain.contractOwner(), "active");
    }
}

module.exports = QuizContract;